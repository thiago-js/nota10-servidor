<?php
	
Class Conexao{
	private $db_host='127.0.0.1';
	private $db_database='nota10';
	private $db_usuario='root';
	private $db_password='';
	private $isConected = false;

	function abrir(){
		try{
			# Informa qual o conjunto de caracteres será usado.
			header('Content-Type: text/html; charset=utf-8');

			$db = $this->isConected = mysqli_connect($this->db_host,$this->db_usuario,$this->db_password)
			or die ("Erro ao conectar com o Banco de dados".mysql_error());
		
			mysqli_select_db($this->isConected, $this->db_database)
			or die ("Erro ao selecionar a Base de dados".mysql_error());

			# Aqui está o segredo
			$db->query("SET NAMES 'utf8'");
			$db->query('SET character_set_connection=utf8');
			$db->query('SET character_set_client=utf8');
			$db->query('SET character_set_results=utf8');

			$db->autocommit(TRUE);

			return $db;
		}catch(Execption $e){
			echo $e;
		}

		return null;
	}
}

$con = new Conexao;
$db = $con->abrir();

?>