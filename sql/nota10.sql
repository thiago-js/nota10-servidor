-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 13-Mar-2016 às 06:09
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nota10`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `estudante`
--

CREATE TABLE IF NOT EXISTS `estudante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar` int(11) NOT NULL DEFAULT '1',
  `nome` varchar(255) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `estudante`
--

INSERT INTO `estudante` (`id`, `avatar`, `nome`, `data_cadastro`) VALUES
(1, 1, 'Thiago', '2016-03-13 04:26:26'),
(2, 2, 'Thiago2', '2016-03-13 04:35:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estudante_pontuacao`
--

CREATE TABLE IF NOT EXISTS `estudante_pontuacao` (
  `estudante` int(11) NOT NULL,
  `pontos` int(11) NOT NULL,
  PRIMARY KEY (`estudante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `estudante_pontuacao`
--

INSERT INTO `estudante_pontuacao` (`estudante`, `pontos`) VALUES
(1, 2);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `estudante_pontuacao`
--
ALTER TABLE `estudante_pontuacao`
  ADD CONSTRAINT `estudante_pontuacao_ibfk_1` FOREIGN KEY (`estudante`) REFERENCES `estudante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
