<?php

class Pontuacao extends Application{
	private $tabela = "estudante_pontuacao";
	private $user;

	public function __construct($db, $user){
		parent::__construct($db);
		$this->user = $user;
	}

	function atualizar($estudante, $pontos){
		$pontuacao = $this->verificarSeExiste($estudante);
		
		if(!$pontuacao){
			$this->inserir($estudante, $pontos);
		}else{
			$query = "UPDATE $this->tabela SET pontos = '$pontos' WHERE estudante = '$estudante'";
			$this->db->query($query);
		}
	}

	function inserir($estudante, $pontos){
		$query = "INSERT INTO $this->tabela (estudante, pontos) VALUES ('$estudante', '$pontos')";
		return $this->db->query($query);
	}

	function verificarSeExiste($estudante){
		$query = "SELECT * FROM $this->tabela WHERE estudante = '$estudante'";
		$res = $this->db->query($query);
		return $res->fetch_assoc();
	}

	public function delegate($operation, $request){
		switch ($operation) {
			case 'pontuacao' :
				$this->success('Pontuação atualizada com sucesso');
				if(!$this->user->verificarSeExisteById($request['estudante'])){
					$this->error("Não existe um estudante com esse id");
				}else{
					$this->atualizar($request['estudante'], $request['pontos']);
				}
			break;
		}

		parent::delegate($operation, $request);
	}
}
?>