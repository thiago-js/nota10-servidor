<?php

class Estudante extends Application{
	private $tabela = "estudante";

	function inserir($nome, $avatar, $uuid){
		$query = "INSERT INTO $this->tabela (nome, avatar, uuid, data_cadastro) VALUES ('$nome', '$avatar', '$uuid', now())";
		$res = $this->db->query($query);
		if($res){ // Se conseguiu cadastrar
			$id = $this->db->insert_id;
			$estudante = $this->verificarSeExisteById($id);
		}else{ // Ja existe um estudante com esse nome ou uuid
			$estudante = $this->verificarSeExisteByNome($nome);
			if($estudante){ // Se existe um estudante com esse nome verifica se eh o estudante do request
				if( strcasecmp($estudante['uuid'], $uuid) == 0 ){ // Atualiza o nome se for o mesmo estudante
					$this->update($uuid, $nome, $avatar);
				}else{ // Nao eh possivel atualizar, somente o estudante esse nome pode atualizar
					$estudante = null;
					$this->error("Já existe um estudante com esse nome");
				}
			}else{ 
				/**
			 		* Nao existe nenhum estudante com o nome informado,
			 		* atualiza o cadastro do estudante do request
			 	**/
				$estudante = $this->findByUUID($uuid);
				$this->update($uuid, $nome, $avatar);
			}

			if(isset($estudante)){
				$estudante['avatar'] = $avatar;
				$estudante['nome'] = $nome;
			}
		}

		return $estudante;
	}

	function upateCurrent($uuid, $nome, $avatar, $estudante){
		$this->update($uuid, $nome, $avatar);
		$estudante['nome'] = $nome;
		$estudante['avatar'] = $avatar;
	}

	function update($uuid, $nome, $avatar){
		$query = "UPDATE $this->tabela SET nome = '$nome', avatar = $avatar WHERE uuid = '$uuid'";
		return $this->db->query($query);
	}

	function verificarSeExisteByNome($nome){
		$query = "SELECT * FROM $this->tabela WHERE nome = '$nome'";
		$res = $this->db->query($query);
		return $res->fetch_assoc();
	}

	function findByUUID($uuid){
		$query = "SELECT * FROM $this->tabela WHERE uuid = '$uuid'";
		$res = $this->db->query($query);
		return $res->fetch_assoc();
	}

	function verificarSeExisteById($id){
		$query = "SELECT * FROM $this->tabela WHERE id = '$id'";
		$res = $this->db->query($query);
		return $res->fetch_assoc();
	}

	function listar($limit){
		$query = "SELECT * FROM estudante_pontuacao, estudante WHERE estudante = id ORDER BY pontos DESC LIMIT $limit";
		$result = $this->db->query($query);
		return $this->getRows($result);
	}

	public function delegate($operation, $request){
		switch ($operation) {
			case 'cadastrar':
				if(!isset($request['nome']) || !isset($request['avatar']) || !isset($request['uuid']))
					$this->error('Falta enviar parametros {nome, avatar ou uuid}');
				else{
					$this->success("Estudante cadastrado");
					$this->json['estudante'] = $this->inserir($request['nome'], $request['avatar'], $request['uuid']);
				}
			break;
			case 'listar' :
				$this->success("Lista de estudantes do Nota 10");
				$this->json["estudantes"] = $this->listar(20);
			break;
		}

		parent::delegate($operation, $request);
	}
}
?>