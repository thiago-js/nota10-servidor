<?php

abstract class Application{

	protected $db;
	protected $json;

	public function __construct($db){
		$this->db = $db;
	}

	protected final function getRows($result){	
		$index = 0;

		while ($row = $result->fetch_assoc()) {
			$rows[$index] = $row;
			$index++;
		}

		return (isset($rows))? $rows : null;
	}

	protected final function end($json){
		/*$file = 'log.txt';
		// Open the file to get existing content
		$current = file_get_contents($file);
		$current .= json_encode($json) . '\n';
		// Write the contents back to the file
		file_put_contents($file, $current);*/
	
		exit(json_encode($json));
	}

	protected final function error($msg){
		if(!isset($this->json))
			$this->json = array();
		
		$this->json['code'] = -1;
		$this->json['msg'] = $msg;
	}

	protected final function success($msg){
		$this->json = array(
						"msg" => $msg,
						"code" => 1
						);
	}

	public function delegate($operation, $request){
		if(isset($this->json))
			exit(json_encode($this->json));
	}
}


?>