<?php
	
/*error_reporting(0);
ini_set(“display_errors”, 0 );*/

include("Conexao.php");
include("Application.php");
include("Estudante.php");
include("Pontuacao.php");
include("Subnivel.php");

$estudante = new Estudante($db);
$pontuacao = new Pontuacao($db, $estudante);
$subnivel  = new Subnivel();

if(!isset($_REQUEST['operacao'])){
	$json = array("msg" => "Falta informar a operacao");
	exit(json_encode($json));	
}

$estudante->delegate($_REQUEST['operacao'], $_REQUEST);
$pontuacao->delegate($_REQUEST['operacao'], $_REQUEST);

if(isset($json))
	exit(json_encode($json));
else
	exit("Nao foi possivel montar o JSON");

?>